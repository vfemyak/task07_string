package View;

import tasks.StringUtils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyView {

  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Scanner input = new Scanner(System.in);

  Locale locale;
  ResourceBundle bundle;

  private void setMenu() {

    menu = new LinkedHashMap<>();
    menu.put("1", bundle.getString("1"));
    menu.put("2", bundle.getString("2"));
    menu.put("3", bundle.getString("3"));
    menu.put("4", bundle.getString("4"));
    menu.put("5", bundle.getString("5"));
    menu.put("6", bundle.getString("6"));
    menu.put("Q", bundle.getString("Q"));
  }

  public MyView() {
    locale = new Locale("uk");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    methodsMenu = new LinkedHashMap<>();

    methodsMenu.put("1", this::testStringUtils);
    methodsMenu.put("2", this::internationalizeMenuUkrainian);
    methodsMenu.put("3", this::internationalizeMenuEnglish);
    methodsMenu.put("4", this::internationalizeMenuNorwegian);
    methodsMenu.put("5", this::internationalizeMenuGreek);
    methodsMenu.put("6", this::testRegEx);
  }

  private void testStringUtils() {
    StringUtils utils = new StringUtils();
    utils.addToParameters(11,33,"null",null,Optional.empty());
    System.out.println(utils.concat());
  }

  private void internationalizeMenuUkrainian() {
    locale = new Locale("uk");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    show();
  }

  private void internationalizeMenuEnglish() {
    locale = new Locale("en");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    show();
  }

  private void internationalizeMenuNorwegian() {
    locale = new Locale("no");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    show();
  }

  private void internationalizeMenuGreek() {
    locale = new Locale("el");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    show();
  }

  private void testRegEx() {
    String message = "Hello, I am Vova. So, who are you?(the)";
    System.out.println(message);
    String message2 = message.replaceAll("[aeiouAEIOU]", "_");
    System.out.println(message2);
    String[] mm = message.split("you|the");
      for (String str : mm) {
          System.out.println(str);
    }

    Pattern pattern = Pattern.compile("^[A-Z].*[.!?]$");  //first letter - high register, last - . or ! or ?
    Matcher matcher = pattern.matcher(message);
    System.out.println("The text " + (matcher.matches() ? "":"doesn`t ") + "matches to the pattern");
  }

  //-------------------------------------------------------------------------

  private void outputMenu() {
    System.out.println("\nMENU:");
    for (String key : menu.keySet()) {
      if (key.length() == 1) {
        System.out.println(menu.get(key));
      }
    }
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println("Please, select menu point.");
      keyMenu = input.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("Q"));
  }
}
